<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MY CV</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5a633d75-e039-4913-9e18-ec4656ea2ec7</testSuiteGuid>
   <testCaseLink>
      <guid>bdb46010-6298-4b18-9c44-16cc434011b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b5e023b-b1c4-4c7e-9526-66e5660fcb52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc2d740e-8c9c-484b-abe0-3a4809507625</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de062090-47c5-4fd6-b155-a0d95139186a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bce9303e-34dc-46a6-8060-2afee2ce9647</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7ee3b28-f2db-4982-84ef-a666935f0d90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>577a5622-ff2f-4fc7-9a07-d4d3caaa73e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cded9785-4b1b-4108-900a-3bdf87624e33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>720e0cda-34bf-4ec9-98a7-3120f1ee075b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0dc4c9a-368d-4526-b11d-af96a99ef75e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_10</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5e8e99b-6df3-4401-a2ef-133a30bca4a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_11</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04e054c2-6a44-45d4-8e3a-4cc75723c482</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_12</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4458f31-ce07-4ce2-b14d-6a0b89d44368</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_13</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9689a0f5-39cc-47a7-b5ea-57b844998360</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_14</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb330bff-6688-4340-a15f-ad5097ca353b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_15</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f626814-8e3c-45c6-ae85-d1f2045cc655</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_16</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e895350f-0f27-4fbd-a4a6-cab8558c12bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_17</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99902338-24db-4e15-8049-d4c5d1d66594</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_18</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa99feb9-b8ee-40fa-9033-a1833c4b988e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_19</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf0ace98-f463-4b4b-a10d-003125c1dd19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_20</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8acc7080-8ff5-491d-9376-b795fa4327f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04c9b257-117c-4727-9bfd-eae982b18802</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MY CV/My CV_22</testCaseId>
   </testCaseLink>
</TestSuiteEntity>

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://rumahsiapkerja.com/')

WebUI.click(findTestObject('Object Repository/Page_Rumah Siap Kerja/div_Daftar  Masuk'))

WebUI.setText(findTestObject('Object Repository/Page_Masuk  Daftar - Rumah Siap Kerja/input'), 'rizky.ashari@rumahsiapkerja.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Masuk  Daftar - Rumah Siap Kerja/input_1'), 'tzH6RvlfSTg=')

WebUI.click(findTestObject('Object Repository/Page_Masuk  Daftar - Rumah Siap Kerja/button_Masuk'))

WebUI.click(findTestObject('Object Repository/Page_Masuk  Daftar - Rumah Siap Kerja/div_Daftar Masuk  Gunakan akun google  Atau_e856e0'))

WebUI.click(findTestObject('Object Repository/Page_Rumah Siap Kerja/img'))

WebUI.click(findTestObject('Object Repository/Page_Profilku - Rumah Siap Kerja/button_CV Saya'))

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/div_Tambah Pengalaman kerja'))

WebUI.setText(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/input'), 'Rumah Siap Kerja')

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/input__periode-awal-1'))

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/li_2021'))

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/input_Periode Waktu_periode-awal-2'))

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/li_Januari'))


WebUI.setText(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/input__spesialisasi'), 'Ac')

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/li_Actuarial ScienceStatistics'))

WebUI.setText(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/input__negara'), 'Indo')

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/li_Indonesia'))

WebUI.setText(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/input__provinsi'), 'DKI')

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/li_DKI JAKARTA'))

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/input__industri'))

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/li_Pertanian, Kehutanan dan Perikanan'))

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/input_Tingkat Jabatan_single-select'))

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/li_GM'))

WebUI.setText(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/input_1'), '10000000')

WebUI.setText(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/textarea'), 'Senang')

WebUI.click(findTestObject('Object Repository/Page_CV Saya - Rumah Siap Kerja/button_Simpan'))


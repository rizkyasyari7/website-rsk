import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.stagingURL)

WebUI.click(findTestObject('Object Repository/Page_Rumah Siap Kerja/div_Daftar  Masuk'))

WebUI.click(findTestObject('Object Repository/Page_Masuk  Daftar - Rumah Siap Kerja/a_Lupa Kata Sandi'))

WebUI.setText(findTestObject('Object Repository/Page_Lupa Kata Sandi - Rumah Siap Kerja/input_Alamat E-Mail_lupa-password-email'), 
    'asdasd123@gmail.com')

WebUI.click(findTestObject('Object Repository/Page_Lupa Kata Sandi - Rumah Siap Kerja/button_Submit'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Lupa Kata Sandi - Rumah Siap Kerja/div_Username asdasd123gmail.com not found'))


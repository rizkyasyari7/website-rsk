import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://rumahsiapkerja.com/')

WebUI.click(findTestObject('Object Repository/Page_Rumah Siap Kerja/div_Daftar  Masuk (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Masuk  Daftar - Rumah Siap Kerja/input (1)'), 'rizky.ashari@rumahsiapkerja.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Masuk  Daftar - Rumah Siap Kerja/input_1 (1)'), 'tzH6RvlfSTg=')

WebUI.click(findTestObject('Object Repository/Page_Masuk  Daftar - Rumah Siap Kerja/button_Masuk (1)'))

WebUI.click(findTestObject('Object Repository/Page_Rumah Siap Kerja/a_Rizky (1)'))

WebUI.click(findTestObject('Page_Rumah Siap Kerja/dropdown_profile'))

WebUI.click(findTestObject('Page_Profilku - Rumah Siap Kerja/Ubah-Profil Saya'))

WebUI.setText(findTestObject('Object Repository/Page_Profilku - Rumah Siap Kerja/input_1_2 (1)'), 'Rizky')

WebUI.setText(findTestObject('Object Repository/Page_Profilku - Rumah Siap Kerja/input_1_2_3 (1)'), '08218349580934')

WebUI.click(findTestObject('Page_Profilku - Rumah Siap Kerja/Simpan-Profil Saya'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Profilku - Rumah Siap Kerja/div_Data kamu berhasil disimpan (1)'))

WebUI.closeBrowser()


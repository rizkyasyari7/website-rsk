import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.stagingURL)

WebUI.click(findTestObject('Object Repository/Page_Rumah Siap Kerja/div_Daftar  Masuk'))

WebUI.click(findTestObject('Object Repository/Page_Masuk  Daftar - Rumah Siap Kerja/span_Gunakan akun google'))

WebUI.switchToWindowTitle('Masuk - Akun Google')

WebUI.setText(findTestObject('Object Repository/Page_Masuk - Akun Google/input_rumahsiapkerja.com_identifier'), 'rizky.ashari@rumahsiapkerja.com')

WebUI.click(findTestObject('Object Repository/Page_Masuk - Akun Google/span_Berikutnya'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Masuk - Akun Google/input_Terlalu sering gagal_password'), 
    'Lf6DSELuQZmQ03AY/55b6A==')

WebUI.sendKeys(findTestObject('Object Repository/Page_Masuk - Akun Google/input_Terlalu sering gagal_password'), Keys.chord(
        Keys.ENTER))

WebUI.closeBrowser()

